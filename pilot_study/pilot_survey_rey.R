### Pilot EarAct Survey - participants: high school students, "havo" educational level, second study year.

# A school class of high school students, after the EarAct survey, completed the pilot survey about their experience with the EarAct survey.

data <- read.csv("survey_34148.csv", quote = "'\"", na.strings=c("", "\"\""), stringsAsFactors=FALSE, fileEncoding="UTF-8-BOM")
data <- data[complete.cases(data[ , c('pilotSurvey1')]), ]
View(data)

### A. Recode variables:
library(tidyverse)
# With dplyr recode function using backquotes as arguments it is necessary to add "L" to a numerical value.
data <- data %>% 
  mutate_at(c("pilotSurvey3","pilotSurvey5", "pilotSurvey7", "pilotSurvey9", "pilotSurvey15"), 
            funs(recode(., `1` = -2L, `2` = -1L, `3` = 0L, `4` = 1L, `5` = 2L)))

### B. Descriptives:

## gender of the participants:
attributes(data)$variable.labels[2] <- "Ik ben een...."
data[, 2] <- factor(data[, 2], levels=c(0,1),labels=c("vrouw", "man"))

plyr::count(data$gender)# 11 male (40.7%), 15 female (55.6%), 1 other (3.7%).

summarytools::ctable(
  x = data$gender,
  y = data$id,
  prop = "t") # total proportions

## age of the participants:
summary(data$leeftijd)
# Min.  Median  Mean  Max. 
# 13.00 13.00   13.41 14.00   
round(sd(data$leeftijd),2)
# 0.50


## pilotSurvey1: Welk apparaat gebruikte je om de vragenlijst in te vullen?
attributes(data)$variable.labels[12] <- "Welk apparaat gebruikte je om de vragenlijst in te vullen?"
data[, 12] <- factor(data[, 12], levels=c(1,2,3,4),labels=c("een smartphone", "een laptop", "een PC", "een tablet"))

plyr::count(data$pilotSurvey1)# # PC (n=22, 81.5%), laptop (n=3, 11.1%), other (n=2, 7.4%).

ctable(
  x = data$pilotSurvey1,
  y = data$id,
  prop = "t") 

## calculate M + SD and min. & max. value for each of the 5-point Likert scale items:
summary(data) # all items at once.

## pilotSurvey3: Hoe was de weergave van de vragenlijst op het apparaat dat je gebruikte?
summary(data$pilotSurvey3)
# Min.  Median  Mean  Max. 
# 1.00  2.00    1.63  2.00   
round(sd(data$pilotSurvey3),2)
# 0.49

## pilotSurvey5: Was de vragenlijst eenvoudig of moeilijk in te vullen op het apparaat dat je gebruikte?
summary(data$pilotSurvey5)
# Min.  Median Mean  Max. 
# 0.00  2.00   1.56  2.00 
round(sd(data$pilotSurvey5),2)
# 0.64

# pilotSurvey7: Hoe duidelijk vond je de vragen? 
summary(data$pilotSurvey7)
# Min.  Median Mean  Max. 
# 0.00  2.00   1.19  2.00 
round(sd(data$pilotSurvey7),2)
# 0.62

# pilotSurvey9: Hoe duidelijk vond je de uitleg en instructies bij de vragen?
summary(data$pilotSurvey9)
# Min.  Median Mean  Max. 
# 0.00  1.00   1.185 2.00 
round(sd(data$pilotSurvey9),2)
# 0.48

# pilotSurvey11: Kon je de antwoorden geven die je wilde geven?
summary(data$pilotSurvey11)
# Min.  Median Mean  Max. 
# 3.00  4.00   4.37  5.00 
round(sd(data$pilotSurvey11),2)
# 0.63

# pilotSurvey15: Wat vind je van de tijd die het duurde om de vragenlijst in te vullen?
summary(data$pilotSurvey15)
# Min.   Median  Mean     Max. 
# -2.00  0.00    -0.148   1.00 
round(sd(data$pilotSurvey15),2)
# 0.66

# 16.	pilotSurvey16(n=6): Hoeveel tijd (in minuten) wil jij maximaal met een vragenlijst als deze bezig zijn?
summary(data$pilotSurvey16, na.rm = TRUE)
# Min.   Median  Mean    Max. 
# 4.00   7.500   8.167   15.000 
round(sd(data$pilotSurvey15, na.rm = TRUE),2)
# 0.66

# 17.	pilotSurvey17 (n=11): Hoe kan de vragenlijst nog verbeterd worden?
data$pilotSurvey17
# See output (open answer option).                                                                                                      

# Time to complete survey (including the additional pilot questions):
summary(data$interviewtime)/60
# Min.  Median  Mean   Max. (in seconds)
# 507.0 883.0   827.7  1136.2 
# Min.  Median  Mean   Max. (in minutes)
# 8.45  11.39   13.79  18.94 
round(sd(data$interviewtime),2)/60
# 2.9505

plot(data$interviewtime/60, xlab = "Student (count)", ylab = "Time to complete survey (in minutes)")
# no outliers.